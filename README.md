Access Code is a PHP application that helps to create a random access code for a door system access. The application is built with a default rules which can also be adjusted to preferences of the developer.

### Features for the application
- Generate a unique with no duplicate from the database
- Generated codes are persisted
- While generating, code can be allocated to a particular user or/and a category.
- A used code can be reset for use and can be reallocated to new or old user or/and category
- Comprises of a default 6-digit code which can be extended up to 9-digit length
- Code default rules are set to check for a void on palindrome and the rule can be changed while generating.
- For any length of code generated, character repetition can be set to not more that 1 character repetition  at least, default is  not more than 3 repetition.
- Access code has default at least 3 character sequence length which can also be set to user preferences with at least 1 sequence length.
- Setup can be made with an adjustment to increase and decrease the default at least 3 unique characters up to at least 1 unique character.
- Expiration date can be set.
- Access code can be set to multiple usage, with number of usage it can be used.
- Code can be set to automatically deletion after usage.


## Help and docs

Package can be access through composer package website and Git repository:

- [Gitlab](https://gitlab.com/atoman08/atoman_access_code)
- [Packagist](https://packagist.org/packages/atoman/access-code)


## Installing atoman\access-code

The recommended way to install atoman\access-code is through
[Composer](https://getcomposer.org/).

```bash
composer require atoman/access-code
```

#### Instruction after requiring the package

Check if the package is automatically discovered on your Laravel application.
- Navigate to config/app.php.
- Check if ```Atoman\AccessCode\AccessCodeServiceProvider::class,``` is added into your Provider section.
- Paste ```Atoman\AccessCode\AccessCodeServiceProvider::class,``` inside you provider section if not already added.
- Go to the root of your Application, on terminal/Command Prompt, run ```php artisan vendor:publish -- force``` to help publish some publishable files like Migration file and config of the package.
- Check for Provider ```Atoman\AccessCode\AccessCodeServiceProvider``` to be published to your application.
- Select ```Atoman\AccessCode\AccessCodeServiceProvider``` to publish accessCode.php and Migration files.
- Run migration ```php artisan migrate``` to migrate package migration file to your database.

## Usage Intructions

````php
use Atoman\AccessCode\App\Http\Services\Access;

public function (Access $access){
    $accessCode = $access->accessCode();     // Generate access code (default length 6)
    $accessCode = $access->accessCode(8);     // Generate access code (defining length 8 ) Hint: defined length must always be >= default 6
}                                                   

````

### Adjustment to default rules when generating Code 

````php
use Atoman\AccessCode\App\Http\Services\Access;

public function (){
    $accessCode = new Access();

    $accessCode->setCharacterRepeat(4);                     //setting value for repetition character  
    $accessCode->setCharacterUniqueness(4);                 //setting value for uniqueness
    $accessCode->setPalindrome(false);                      // set if palindrome should be check
    $accessCode->setSequenceLength(4);                      //set length of sequence

    $accessCode = $access->accessCode(8);                    // Generate access code (defining length 8 )
}                                                   

````

## Accessing the Interface

You can access the interface for the package while your application is running, navigate to your http://localhost:8000/access to view the interface.

You will have access to Documentation page, Create Code Page, List all code page, Confirm Code page, Assign Code page.




## Version Guidance
- [atoman-access-code-repo v1.1.0](https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.1.0?ref_type=tags) 
- [atoman-access-code-repo v1.0.2](https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.0.2?ref_type=tags) 
- [atoman-access-code-repo v1.0.1](https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.0.1?ref_type=tags) 
- [atoman-access-code-repo v1.0.0](https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.0.0?ref_type=tags) 


## Security

If you discover a security vulnerability within this package, please send an email to atoworldinc@gmail.com or ato4life2008@gmail.com. All security vulnerabilities will be promptly addressed. Please do not disclose security-related issues publicly until a fix has been announced. 

## License

atoman\access-code is made available under the MIT License (MIT). Please see [License File](LICENSE) for more information.

