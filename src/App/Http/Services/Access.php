<?php

namespace Atoman\AccessCode\App\Http\Services;

class Access extends GenerateCode

{

    public $accessCode;
    public $assigned_to;
    public $category;

    public function confirmCode($accessCode){
        $this->accessCode = $accessCode;
    }


    public function accessCode(int $length=6){
        return $this->createCode($length);
    }




}
