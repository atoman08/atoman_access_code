<?php

namespace Atoman\AccessCode\App\Http\Services;

use Atoman\AccessCode\Models\AccessCode;

    class GenerateCode
    {
        public $length;
        public $character_repeat = 3 ;
        public $palindrome = true;
        public $sequence_length = 3;
        public $unique_character = 3;
        public $accessCode;

        
        /**
         * Create access Code
         *
         * @return void
         */
        public function createCode(int $length=6){
            $this->length = $length < 6 ? 6 : $length;
            $this->generateCode();

            if($this->checkForPalindrome()) $this->createCode();
            if(!$this->checkForCharacterRepetition()) $this->createCode();
            if(!$this->checkForCharacterSequence()) $this->createCode();
            if(!$this->checkForUniqueCharacter()) $this->createCode();
            if(!$this->checkForDatabaseUniqueness()) $this->createCode();

            return $this->accessCode;
        }

        /**
         * Sets the access code for the object.
         *
         * @param string $accessCode The access code to set.
         */
        public function setAccessCode(string $accessCode):void{
            $this->accessCode = $accessCode;
        }

        /**
         * Set the number of times a character should be repeated.
         *
         * @param int $repeat The number of times a character should be repeated.
         * @return void
         */
        public function setCharacterRepeat(int $repeat):void{
            $this->character_repeat = $repeat;
        }

        /**
         * Sets the length of the sequence.
         *
         * @param int $sequence The length of the sequence.
         * @return void
         */
        public function setSequenceLength(int $sequence):void{
            $this->sequence_length = $sequence;
        }

        /**
         * Set the value of the palindrome property.
         *
         * @param bool $palindrome The new value for the palindrome property.
         * @return void
         */
        public function setPalindrome(bool $palindrome):void{
            $this->palindrome = $palindrome;
        }

        /**
         * Set the uniqueness of the character.
         *
         * @param int $uniqueness The uniqueness value to be set.
         * @return void
         */
        public function setCharacterUniqueness(int $uniqueness):void{
            $this->unique_character = $uniqueness;
        }

        /**
         * Generate Random numbers
         *
         * @return void
         */
        public function generateCode():void{
            $this->accessCode = mt_rand(10 ** ($this->length - 1), (10 ** $this->length) - 1);
        }

        /**
         * Check if access code not Palindrome
         * Only if Palidrome is set to True
         *
         * @return boolean
         */
        public function checkForPalindrome():bool{
            //Check if palidrome set is false, then return true to continue without checking palindrome
            if($this->palindrome === false ) return false;

            $realStr = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $this->accessCode)); // Convert to lowercase and remove non-alphanumeric characters
            $reversed = strrev($realStr); // Reverse the string

            if($realStr === $reversed) return true;

            return false;
        }

        /**
         * Check for Character repetition with value set inside $this->character_repeat
         *
         * @return boolean
         */
        public function checkForCharacterRepetition():bool{

            //Check if character repeat is set and not also equal to 0, return true to continue without checking character repetition
            if(is_null($this->character_repeat) || $this->character_repeat < 1) return true;

            $characterCount = array();
            foreach (str_split($this->accessCode) as $char) {
                if (!isset($characterCount[$char])) {
                    $characterCount[$char] = 1;
                } else {
                    $characterCount[$char]++;
                    if ($characterCount[$char] > $this->character_repeat) {
                        return false;
                    }
                }
            }

            return true;
        }

        /**
         * Check for Character sequence with value set inside $this->sequence_length
         *
         * @return boolean
         */
        public function checkForCharacterSequence():bool{

            //Check if character sequence is set and not also equal to 0, return true to continue without checking character sequence count
            if(is_null($this->sequence_length) || $this->sequence_length < 1) return true;

            $sequence = '';
            foreach (str_split($this->accessCode) as $char) {
                if ($sequence === $char) {
                    $sequence .= $char;
                    if (strlen($sequence) > $this->sequence_length) {
                        return false;
                    }
                } else {
                    $sequence = $char;
                }
            }
            return true;
        }

        /**
         * Check for Unique Character count with value set inside $this->unique_character;
         *
         * @return boolean
         */
        public function checkForUniqueCharacter():bool{

            //Check if character sequence is set and not also equal to 0, return true to continue without checking unique character count
            if(is_null($this->unique_character) || $this->unique_character < 1) return true;
            if(count(array_unique(str_split($this->accessCode))) >= $this->unique_character) return true;
            return false;
        }

        /**
         * Check for Database Uniqueness
         *
         * @return boolean
         */
        public function checkForDatabaseUniqueness():bool{
            $result = AccessCode::where('code', $this->accessCode)->first();
            if(isset($result)) return false;
            return true;
        }




    }



