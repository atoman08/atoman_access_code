<?php

namespace Atoman\AccessCode\App\Http\Controllers;

use Atoman\AccessCode\Models\AccessCode;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ConfirmCodeController extends AccessCodeController
{

    private $result;
    public $assigned_to;
    public $category;
    public $code;

    /**
     * Confirm the request.
     *
     * @param Request $request The request object.
     * @throws \Throwable If an error occurs.
     * @return mixed The result of the checkValidity() method.
     */
    public function confirm(Request $request){

        $validation = Validator::make($request->all(), [
            'code'  => "required|string",
        ]);

        if($validation->fails()) return $this->errorResponse('Invalid validation request', $validation->messages()->all(), Response::HTTP_BAD_REQUEST);

        try {
            //code...
            $this->code                 = $this->cleanString($request->code);
            $this->assigned_to          = is_null($request->assigned_to) ? null : $this->cleanString($request->assigned_to);
            $this->category             = is_null($request->category) ? null : $this->cleanString($request->category);
            $this->result               =  AccessCode::where('code', $this->code)->where('status', true)->first();

            return $this->checkValidity();

        } catch (\Throwable $th) {
            //throw $th;
            return $this->errorResponse('Error Occurred', $th->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Do a check to confirm if the user assigned to is valid for the code
     *
     * @return boolean
     */
    public function confirmAssignedTo():bool{
        if(!is_null($this->result->assigned_to) && $this->result->assigned_to != $this->assigned_to) return false;
        return true;
    }

    /**
     * Do a check to confirm if the category set is valid for the code
     *
     * @return boolean
     */
    public function confirmCategory():bool{
        if(!is_null($this->result->category) && $this->result->category != $this->category) return false;
        return true;
    }

    /**
     * Do confirmation if the code is reusebaled and
     * has not exceed the reusabled time
     *
     * @return boolean
     */
    public function checkReuseable():bool{
        if($this->result->total_useable < $this->result->reuseable_count) return true;
        return false;
    }

    /**
     * Do confirmation for expiration of the code
     *
     * @return boolean
     */
    public function checkExpiration():bool{
        if(!is_null($this->result->expires_at) && time() > strtotime($this->result->expires_at)) return false;
        return true;
    }



    public function checkValidity(){

        $code = AccessCode::where('code', $this->code)->first();

        // Check if result for code is set
        if(is_null($this->result)) return $this->errorResponse('Invalid code', [], Response::HTTP_BAD_REQUEST);

        // Confirm if assigned to is valid
        if(!$this->confirmAssignedTo()) return $this->errorResponse('Code not assigned to user', [], Response::HTTP_UNAUTHORIZED);

        // confirm if category is valid
        if(!$this->confirmCategory()) return $this->errorResponse('Code not assigned to category', [], Response::HTTP_UNAUTHORIZED);

        // confirm if the code has not exceed the reuseable time
        if(!$this->checkReuseable()) {
            $code->update(['status' => false]);
            return $this->errorResponse('Code useable count exceeded', [], Response::HTTP_UNAUTHORIZED);
        }

        // Check if the code already expired and update record if expired
        if(!$this->checkExpiration()) {
            $code->update(['status' => false]);
            return $this->errorResponse('Code expiration date due', [], Response::HTTP_UNAUTHORIZED);
        }

        // increase total useabled
        $code->update(['total_useable' => $code->total_useable + 1]);

        // Delete record of delete_after_use is set to true
        if($this->result->delete_after_use) $code->delete();

        return $this->successResponse('Code Confirmed successfully', $this->result, Response::HTTP_ACCEPTED);


    }


}
