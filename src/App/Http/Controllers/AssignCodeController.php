<?php

namespace Atoman\AccessCode\App\Http\Controllers;

use Atoman\AccessCode\Models\AccessCode;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class AssignCodeController extends AccessCodeController{

    private $result;
    public $assigned_to;
    public $category;
    public $code;

    /**
     * Assign a request to a user.
     *
     * @param Request $request The request object containing the data to be assigned.
     * @throws \Illuminate\Validation\ValidationException If the validation fails.
     * @return mixed The result of the assignment.
     */
    public function assign(Request $request){

        $validation = Validator::make($request->all(), [
            'code'  => "required|string",
        ]);

        if($validation->fails()) return $this->errorResponse('Invalid validation request', $validation->messages()->all(), Response::HTTP_BAD_REQUEST);

        try {
            //code...
            $this->code                 = $this->cleanString($request->code);
            $this->assigned_to          = is_null($request->assigned_to) ? null : $this->cleanString($request->assigned_to);
            $this->category             = is_null($request->category) ? null : $this->cleanString($request->category);
            $this->result               =  AccessCode::where('code', $this->code)->first();

            return $this->reAssign();

        } catch (\Throwable $th) {
            //throw $th;
            return $this->errorResponse('Error Occurred', $th->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Reassigns the code to a different user or category.
     *
     * @throws Some_Exception_Class Invalid code
     * @return Some_Return_Value The response message and the updated code
     */
    public function reAssign(){
        if(is_null($this->result))return $this->errorResponse('Invalid code', [], Response::HTTP_BAD_REQUEST);

        $arr = ['status' => true, 'total_useable' => 0];
        if(!is_null($this->assigned_to)) array_push($arr, ['assigned_to' => $this->assigned_to]);
        if(!is_null($this->category)) array_push($arr, ['category' => $this->category]);

        $this->result->update($arr);

        return $this->successResponse('Code assigned successfully', AccessCode::where('code', $this->result->code)->first(), Response::HTTP_OK);
    }
}
