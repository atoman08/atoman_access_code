<?php

namespace Atoman\AccessCode\App\Http\Controllers;

use Atoman\AccessCode\Models\AccessCode;
use Illuminate\Http\Response;
use Atoman\AccessCode\App\Http\Controllers\AccessCodeController;
use Atoman\AccessCode\App\Http\Services\Access;
use Atoman\AccessCode\App\Http\Services\GenerateCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreateCodeController extends AccessCodeController{

    private $code;
    private $category;
    private $setup_mode;
    private $reuseable;
    private $reuseable_count;
    private $assigned_to;
    private $total_useable = 0;
    private $delete_after_use;
    private $expires_at;
    public $sequence = 3;
    public $palindrome = true;
    public $repeat = 3;
    public $unique = 3;
    public $length = 6;

    /**
     * Stores the request data and generates an access code.
     *
     * @param Request $request The request data.
     * @throws Throwable If an error occurs during the execution.
     * @return Response The response object.
     */
    public function store(Request $request,Access $access){

        $validation = Validator::make($request->all(), [
            'mode'                      => "required|string",
            'reuseable_count'           => "required_unless:reuseable,null",
            'code_length'               => "required_unless:mode,default",
            //'code_is_palindrome'        => "required_unless:mode,default",
            'code_character_sequence'   => "required_unless:mode,default",
            'code_character_uniqueness' => "required_unless:mode,default",
            'code_character_repetition' => "required_unless:mode,default",

        ]);

        if($validation->fails()) return $this->errorResponse('Invalid validation request', $validation->messages()->all(), Response::HTTP_BAD_REQUEST);

        try {
            //code...

            $this->category             = is_null($request->category) ? null : $this->cleanString($request->category);
            $this->assigned_to          = is_null($request->assigned_to) ? null : $this->cleanString($request->assigned_to);
            $this->reuseable            = is_null($request->reuseable) ? false : true;
            $this->reuseable_count      = is_null($request->reuseable_count) ? 1 : (int) $request->reuseable_count;
            $this->delete_after_use     = is_null($request->delete_after_use) ? false : true;
            $this->expires_at           = is_null($request->expires_at) ? null : date("d/M/Y h:i:s", strtotime($request->expires_at));

            $mode                       = $this->cleanString($request->mode);

            if(strtolower($mode) != 'default'){
                $this->sequence         = is_null($request->code_character_sequence) ? $this->sequence : (int) $request->code_character_sequence;
                $this->unique           = is_null($request->code_character_uniqueness) ? $this->unique : (int) $request->code_character_uniqueness;
                $this->repeat           = is_null($request->code_character_repetition) ? $this->repeat : (int) $request->code_character_repetition;
                $this->length           = is_null($request->code_length) ? $this->length : (int) $request->code_length;
                $this->palindrome       = is_null($request->code_is_palindrome) ? $this->palindrome : false;

                $access->setCharacterRepeat($this->repeat);
                $access->setCharacterUniqueness($this->unique);
                $access->setPalindrome($this->palindrome);
                $access->setSequenceLength($this->sequence);
            }

            if($this->length > 9) return $this->errorResponse("Length must not be greater than 9", [], Response::HTTP_BAD_REQUEST);
            if($this->unique > intval($this->length/2)) return $this->errorResponse("Unique length must not be greater than 1/2 of ".$this->length, [], Response::HTTP_BAD_REQUEST);
            if($this->repeat > intval($this->length/2)) return $this->errorResponse("Character repeat length must not be greater than 1/2 of ".$this->length, [], Response::HTTP_BAD_REQUEST);
            if($this->sequence > intval($this->length/2)) return $this->errorResponse("Character sequence length must not be greater than 1/2 of ".$this->length, [], Response::HTTP_BAD_REQUEST);

            $this->setup_mode = [
                'palindrome'            => $this->palindrome,
                'character_sequence'    => $this->sequence,
                'character_uniqueness'  => $this->unique,
                'character_length'      => $this->length,
                'character_repeat'      => $this->repeat
            ];

            //$this->code = $generateCode->createCode();
            $this->code = $access->accessCode($this->length);

            $this->addRecord();
            return $this->successResponse('Access code generated successfully', [AccessCode::where('code', $this->code)->first()], Response::HTTP_CREATED);

        } catch (\Throwable $th) {
            //throw $th;
            return $this->errorResponse('Error Occurred', $th->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function addRecord():void{

        AccessCode::create([
            'code'              => $this->code,
            'category'          => $this->category,
            'setup_mode'        => $this->setup_mode,
            'reuseable'         => $this->reuseable,
            'reuseable_count'   => $this->reuseable_count,
            'assigned_to'       => $this->assigned_to,
            'total_useable'     => $this->total_useable,
            'delete_after_use'  => $this->delete_after_use,
            'expires_at'        => $this->expires_at
        ]);

    }

}
