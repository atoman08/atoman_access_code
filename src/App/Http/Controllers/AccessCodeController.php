<?php

namespace Atoman\AccessCode\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Atoman\AccessCode\Models\AccessCode;

class AccessCodeController extends Controller
{

    public function landingPage(){
        return view('access-code::index');
    }

    public function createPage(){
        return view('access-code::create-access-code');
    }

    public function listPage(){
        $codes = AccessCode::orderBy('created_at', 'desc')->paginate(15) ?? [];
        return view('access-code::list-access-code', compact('codes'));
    }

    public function confirmPage(){
        return view('access-code::confirm-access-code');
    }

    public function assignPage(){
        return view('access-code::assign-access-code');
    }

    public function successResponse($message, $data, $code){
        return response()->json([
            'message' => $message,
            'data'  => $data
        ], $code);
    }

    public function errorResponse($message, $errors, $code){
        return response()->json([
            'message' => $message,
            'errors'  => $errors
        ], $code);
    }

    public function cleanString($str){
        return addslashes(strip_tags($str));
    }

}
