<?php

$namespace = "Atoman\AccessCode\App\Http\Controllers";

use Illuminate\Support\Facades\Route;


Route::group([
    'namespace'     => $namespace,
    'prefix'        => 'access',
    'middleware'    => 'web'
], function(){

    Route::get('/', 'AccessCodeController@landingPage')->name('atoman.access-code.landing-page');
    Route::get('/create', 'AccessCodeController@createPage')->name('atoman.access-code.create-page');
    Route::get('/list', 'AccessCodeController@listPage')->name('atoman.access-code.list-page');
    Route::get('/confirm', 'AccessCodeController@confirmPage')->name('atoman.access-code.confirm-page');
    Route::get('/reassign', 'AccessCodeController@assignPage')->name('atoman.access-code.reassign-page');

    Route::post('/create', 'CreateCodeController@store')->name('atoman.access-code.create');
    Route::post('/confirm', 'ConfirmCodeController@confirm')->name('atoman.access-code.confirm');
    Route::post('/reassign', 'AssignCodeController@assign')->name('atoman.access-code.reassign');
});


