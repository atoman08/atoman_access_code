<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('access_codes', function (Blueprint $table) {
            $table->id();
            $table->string('code', 64)->unique();
            $table->string('category')->nullable();
            $table->text('setup_mode')->nullable();
            $table->boolean('reuseable')->default(false);
            $table->integer('reuseable_count')->default(0);
            $table->string('assigned_to')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('total_useable')->default(0);
            $table->boolean('delete_after_use')->default(false);
            $table->timestamp('last_used_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('access_codes');
    }
};
