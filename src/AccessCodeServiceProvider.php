<?php

namespace Atoman\AccessCode;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AccessCodeServiceProvider extends ServiceProvider
{

    /**
     * Boot services
     *
     * @return void
     */
    public function boot(){
        Schema::defaultStringLength(191);
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/./../pages/views', 'access-code');
    }


     /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->registerPublishes();
    }

    private function registerPublishes(){

        $publishArray = [
            'migrations' => [
                __DIR__."/publishes/database/migrations" => database_path('migrations'),
            ],
            'config' => [
                __DIR__."/publishes/config/accessCode.php" => config_path('accessCode.php'),
            ],
        ];

        foreach($publishArray as $grp => $path){
            $this->publishes($path, $grp);
        }

    }



}
