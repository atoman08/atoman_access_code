<?php

namespace Atoman\AccessCode\Models;

use Illuminate\Database\Eloquent\Model;

class AccessCode extends Model{

    protected $fillable = [
        'code',
        'category',
        'setup_mode',
        'reuseable',
        'reuseable_count',
        'assigned_to',
        'status',
        'total_useable',
        'delete_after_use',
        'last_used_at',
        'expires_at'
    ];

    protected $casts = [
        'reuseable_count'       => 'int',
        'total_useable'         => 'int',
        'reuseable_count'       => 'int',
        'status'                => 'boolean',
        'setup_mode'            => 'array'
    ];

    public function setSetupModeAttributes($value){
        $this->attributes['setup_mode'] = json_decode($value);
    }
}
