<?php

declare (strict_types=1);

use Atoman\AccessCode\App\Http\Services\Access;
use Atoman\AccessCode\App\Http\Services\GenerateCode;
use PHPUnit\Framework\TestCase;

class AccessCodeTest extends TestCase{

    /**
     * Test the functionality of the testPalindrome method.
     *
     * @throws Exception If the test fails.
     */
    public function testPalindrome(){
        $access = new Access();
        $access->setAccessCode('223322');
        $this->assertEquals(true, $access->checkForPalindrome(), "Access Code is Palindrome");
    }

    /**
     * Test if the generated code is not a palindrome.
     *
     * @throws Exception if the access code is a palindrome.
     */
    public function testNotPalindrome(){

        $access = new Access();
        $access->setPalindrome(false);
        $access->setAccessCode('223323');

        $this->assertNotEquals(true, $access->checkForPalindrome(), "Access Code is not Palindrome");
    }

    /**
     * Test the uniqueness of characters in the access code.
     *
     * @return void
     */
    public function testNot3UniquenessCharacters(){

        $access = new Access();
        $access->setAccessCode('223323');
        $this->assertEquals(false, $access->checkForUniqueCharacter(), "Access Code character does not have 3 unique characters");
    }

    /**
     * Generate the function comment for the given function body in a markdown code block with the correct language syntax.
     *
     * @throws Some_Exception_Class description of exception
     * @return void
     */
    public function testHas3UniquenessCharacters(){

        $access = new Access();
        $access->setAccessCode('223326');
        $this->assertEquals(true, $access->checkForUniqueCharacter(), "Access Code character has 3 unique characters");
    }

    /**
     * Test case for the testHas3SequenceCharacters function.
     *
     * @return void
     */
    public function testHasMoreThan3SequenceCharacters(){
        $access = new Access();
        $access->setAccessCode('222226');
        $this->assertEquals(true, $access->checkForCharacterSequence(), "Access Code character has 3 sequence characters.");
    }

    /**
     * Test if the function checkForCharacterSequence() correctly identifies access codes that do not have 3 sequence characters.
     *
     * @throws PHPUnit\Framework\AssertionFailedError if the assertion fails
     * @return void
     */
    public function testNotMoreThan3SequenceCharacters(){

        $access = new Access();
        $access->setAccessCode('223326');
        $this->assertEquals(true, $access->checkForCharacterSequence(), "Access Code character does not have 3 sequence characters");
    }

    public function testNotMoreThan3RepeatedCharacters(){

        $access = new Access();
        $access->setAccessCode('223326');
        $this->assertEquals(true, $access->checkForCharacterRepetition(), "Access Code character doesnt not have more than 3 repeated characters");
    }

    public function testHasMoreThan3RepeatedCharacters(){

        $access = new Access();
        $access->setAccessCode('222206');
        $this->assertNotEquals(true, $access->checkForCharacterRepetition(), "Access Code character has more than 3 repeated characters");
    }
}
