
@extends("access-code::template.master")

@section('title', 'List Code - Access Code Documentation')
@section('list', 'active')

@section('body')
<div class="mx-auto col-lg-10 col-10 pt-2 mt-2">
    <div class="row text-center mb-2">
        <h4>List Access Code</h4>
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                    <td>Code</td>
                    <td>Assigned/Category</td>
                    <td>Setup Mode</td>
                    <td>Reuseable</td>
                    <td>Reuseable Count</td>
                    <td>Total Used</td>
                    <td>Delete after Use</td>
                    <td>Expired Date</td>
                    <td>Status</td>
                    <td>Created At</td>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($codes as $code )
                        <tr>
                            <td>{{$code->code}}</td>
                            <td>{{$code->assigned_to? $code->assigned_to : '-'}}/{{$code->category ? $code->category : '-'}}</td>
                            <td>
                                <small>
                                <?php
                                if($code->setup_mode){
                                    $mode = json_decode(json_encode($code->setup_mode), true);
                                    echo "Palindrome =>".$mode['palindrome'] ."<br/>";
                                    echo "Sequence =>".$mode['character_sequence'] ."<br/>";
                                    echo "Unique =>".$mode['character_uniqueness'] ."<br/>";
                                    echo "Repeat =>".$mode['character_repeat'] ."<br/>";
                                    echo "Length =>".$mode['character_length'] ."<br/>";
                                }
                                ?>
                                </small>

                            </td>
                            <td>{{$code->reuseable== true ? 'Yes' : 'No'}}</td>
                            <td>{{$code->reuseable_count}}</td>
                            <td>{{$code->total_useable}}</td>
                            <td>{{$code->delete_after_use == true ? 'Yes' : 'No'}}</td>
                            <td>{{$code->expires_at ? date("d M Y", strtotime($code->expires_at)) : '-'}}</td>
                            <td>{{$code->status == true ? 'Active' : 'Inactive'}}</td>
                            <td>{{date("d M Y", strtotime($code->created_at))}}</td>
                        </tr>
                        @endforeach
                </tbody>

                {{$codes->render()}}
            </table>
        </div>
    </div>

</div>
@endsection




