
<header>
    <nav class="navbar navbar-expand-lg bg-body-tertiary bg-dark border-bottom border-body">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">AccessCode</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link @yield('documentation')" aria-current="page" href="{{route('atoman.access-code.landing-page')}}">Documentation</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" @yield('create') aria-current="page" href="{{route('atoman.access-code.create-page')}}">Create Code</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" @yield('list') aria-current="page" href="{{route('atoman.access-code.list-page')}}">List Code</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" @yield('confirm') aria-current="page" href="{{route('atoman.access-code.confirm-page')}}">Confirm Code</a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  @yield('reassign') aria-current="page" href="{{route('atoman.access-code.reassign-page')}}">Reassign Code</a>
              </li>

            </ul>
          </div>
        </div>
    </nav>
</header>
