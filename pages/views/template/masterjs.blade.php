
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script>

$(document).on("keyup", ".only_numeric", function() {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

function alertBox(message, status){
    if(status == 'success') statusClass = 'bg-success';
    else if(status == 'error') statusClass = 'bg-danger';
    $('.alert').html(`<div class="mx-auto p-2 rounded ${statusClass} text-white">${message}</div>`).fadeIn(100).fadeOut(5000)
}

$(document).on('submit', '#formHandle', function(e) {

e.preventDefault();
var svgDis = '<svg version="1.1" style="width: 50px; height: 10px; margin: 0px; margin-left: -15px; display:inline-block;"  x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 0 0" xml:space="preserve"><circle fill="#fff" stroke="none" cx="6" cy="10" r="6"><animate attributeName="opacity" dur="1s" values="0;1;0" repeatCount="indefinite" begin="0.1"/> </circle><circle fill="#fff" stroke="none" cx="26" cy="10" r="6"><animate attributeName="opacity" dur="1s" values="0;1;0" repeatCount="indefinite" begin="0.2"/> </circle><circle fill="#fff" stroke="none" cx="46" cy="10" r="6"><animate attributeName="opacity" dur="1s" values="0;1;0" repeatCount="indefinite" begin="0.3"/></circle></svg>';

var btnText = $('#formBtn').text();

$('#formBtn').prop('disabled', true).html('Waiting ' + svgDis);

$.ajax({
    method: 'POST',
    url: $(this).attr('action'),
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType: "json",
    success: function(data) {
        alertBox(data.message, 'success');
        //console.log(data);
        $('#formBtn').prop('disabled', false).html(btnText);
        //Scroll to the top
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

    },
    error: function(error) {
        alertBox(error.responseJSON.message, 'error');
        $('#formBtn').prop('disabled', false).html(btnText);
        console.log(error);
    }
});
return false;
});

</script>
