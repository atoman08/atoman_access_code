
@extends("access-code::template.master")

@section('title', 'Reassign Code - Access Code Documentation')
@section('reassign', 'active')

@section('body')
    <div class="mx-auto col-lg-5 col-10 pt-5 mt-5">
        <div class="row text-center mb-4">
            <h4>Reassign Access Code</h4>
        </div>

        <form method="POST" action="{{route('atoman.access-code.reassign')}}" id="formHandle" >
            <div class="alert"></div>
            {{ csrf_field() }}
            <div class="mb-3">
              <label for="code" class="form-label">Access Code <span class="text-danger">*</span></label>
              <input type="text" required class="form-control only_numeric" minlength="6" id="code" name="code" aria-describedby="codeHelp">
              <div id="codeHelp" class="form-text">Access code, minimum of 6 digit</div>
            </div>
            <div class="row form-group">
                <div class="mb-3 col-lg-6">
                    <label for="assigned_to" class="form-label">User Assigned To</label>
                    <input type="text" class="form-control" id="assigned_to" name="assigned_to" aria-describedby="assignHelp">
                    <div id="assignHelp" class="form-text">User assigned if available</div>
                </div>
                <div class="mb-3 col-lg-6">
                    <label for="category" class="form-label">Category Assigned To</label>
                    <input type="text" class="form-control" id="category" name="category" aria-describedby="categoryHelp">
                    <div id="categoryHelp" class="form-text">Category assigned if available</div>
                </div>
            </div>
            <div class="text-center mt-3">
                <button type="submit" id="formBtn" class="btn btn-primary">Reassign Code</button>
            </div>

        <form>
    </div>
@endsection


