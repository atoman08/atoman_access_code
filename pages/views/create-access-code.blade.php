
@extends("access-code::template.master")

@section('title', 'Create Code - Access Code Documentation')
@section('create', 'active')

@section('body')
<div class="mx-auto col-lg-5 col-10 pt-2 mt-2">
    <div class="row text-center mb-2">
        <h4>Create Access Code</h4>
    </div>

    <form method="POST" action="{{route('atoman.access-code.create')}}" id="formHandle" >
        <div class="alert"></div>
        {{ csrf_field() }}
        <div class="mb-3">
          <label for="mode" class="form-label">Generate Rules <span class="text-danger">*</span></label>
          <select required class="form-control" id="mode" name="mode" aria-describedby="modeHelp">
            <option value="">--Select Rule--</option>
            <option value="default">Default Rules</option>
            <option value="customized">Customized</option>
          </select>
          <div id="modeHelp" class="form-text"></div>
        </div>

        <div id="modeDis" class="row form-group border-top border-bottom mt-2 mb-2 pt-2 pb-2">

        </div>

        <div class="row form-group mt-2">
            <div class="mb-3 col-lg-6">
                <label for="assigned_to" class="form-label">User Assigned To</label>
                <input type="text" class="form-control" id="assigned_to" name="assigned_to">
            </div>
            <div class="mb-3 col-lg-6">
                <label for="category" class="form-label">Category Assigned To</label>
                <input type="text" class="form-control" id="category" name="category">
            </div>
        </div>

        <div class="row form-group mt-2">
            <div class="mb-3 col-lg-6">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="reuseable" name="reuseable">
                    <label class="form-check-label" for="reuseable">
                      Code should be Reuseable?
                    </label>
                </div>
            </div>
            <div class="mb-3 col-lg-6 ">
                <label for="reuseable_count" class="form-label">Reuseable Count</label>
                <input type="number" min="1" disabled class="form-control" id="reuseable_count" name="reuseable_count">
            </div>
        </div>

        <div class="row form-group mt-2">
            <div class="mb-3 col-lg-6 ">
                <label for="expires_at" class="form-label">Expiration Date</label>
                <input type="date" class="form-control" id="expires_at" name="expires_at">
            </div>
            <div class="mb-3 col-lg-6">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="last_used_at" name="last_used_at">
                    <label class="form-check-label" for="last_used_at">
                      Code should be delete after use?
                    </label>
                </div>
            </div>
        </div>



        <div class="text-center mt-3">
            <button type="submit" id="formBtn" class="btn btn-primary">Generate Code</button>
        </div>

    <form>
</div>
@endsection

@section('otherJs')
    <script>
        $("#mode").on('change', function(){
            //console.log($(this).val());
            let mode = $(this).val();
            if( mode == 'customized'){
                $("#modeDis").html(`<div class="mb-3 col-lg-6 col-12">
                <label for="code_length" class="form-label">Code Length</label>
                <input type="number" required min="6" class="form-control" id="code_length" name="code_length">
            </div>
            <div class="mb-3 col-lg-6 col-12">
                <label for="code_character_repetition" class="form-label">Character Repetition <span class="text-muted">(at least)</span></label>
                <input type="number" required min="1" class="form-control" id="code_character_repetition" name="code_character_repetition">
            </div>
            <div class="mb-3 col-lg-6 col-12">
                <label for="code_character_sequence" class="form-label">Character Sequence <span class="text-muted">(at least)</span></label>
                <input type="number" required min="1" class="form-control" id="code_character_sequence" name="code_character_sequence">
            </div>
            <div class="mb-3 col-lg-6 col-12">
                <label for="code_character_uniqueness" class="form-label">Character Uniqueness <span class="text-muted">(at least)</span></label>
                <input type="number" required min="1" class="form-control" id="code_character_uniqueness" name="code_character_uniqueness">
            </div>
            <div class="mb-3 col-lg-6 col-12">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="code_is_palindrome" name="code_is_palindrome">
                    <label class="form-check-label" for="code_is_palindrome">
                      Code can be Palindrome?
                    </label>
                </div>
            </div>`);
            }
            else if (mode == 'default'){
                $("#modeDis").html(`
                <div class="p-3 col-lg-6 col-12 bg-secondary text-white">
                <h6>Default Reles</h6>
                <p>
                    Code Lenght: <strong>6</strong><br/>
                    Character Sequence: <strong>3</strong><br/>
                    Character Repitition: <strong>3</strong><br/>
                    Character Uniqeuness: <strong>3</strong><br/>
                    Code Palindrome: <strong>No</strong><br/>
                </p>
            </div>
            `);
            }else{$("#modeDis").empty().html(' ')}
        });

        $("#reuseable").on('click', function(){
            $(this).is(":checked") ? $('#reuseable_count').attr('disabled', false) : $('#reuseable_count').attr('disabled', true);
        });
    </script>
@endsection


