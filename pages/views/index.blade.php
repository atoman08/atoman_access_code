
@extends("access-code::template.master")

@section('title', 'Access Code Documentation')
@section('documentation', 'active')

@section('body')
    <div class="container pt-3">
        <div class="row mx-auto col-lg-12">
            <h4>Welcome to Access Code Documentation</h4>
        </div>

        <p>Access Code is a PHP application that helps to create a random access code for a door system access. The application is built with a default rules which can also be adjusted to preferences of the developer.</p>

        <p>
            <h6>Features for the application</h6>
            <ul>
                <li>Generate a unique with no duplicate from the database</li>
                <li>Generated codes are persisted</li>
                <li>While generating, code can be allocated to a particular user or/and a category.</li>
                <li>A used code can be reset for use and can be reallocated to new or old user or/and category</li>
                <li>Comprises of a default 6-digit code which can be extended up to 9-digit length.</li>
                <li>Code default rules are set to check for a void on palindrome and the rule can be changed while generating.</li>
                <li>For any length of code generated, character repetition can be set to not more that 1 character repetition  at least, default is  not more than 3 repetition.</li>
                <li>Access code has default at least 3 character sequence length which can also be set to user preferences with at least 1 sequence length.</li>
                <li>Setup can be made with an adjustment to increase and decrease the default at least 3 unique characters up to at least 1 unique character.</li>
                <li>Expiration date can be set.</li>
                <li>Access code can be set to multiple usage, with number of usage it can be used.</li>
                <li>Code can be set to automatically deletion after usage.</li>

            </ul>
        </p>
        <h5>Help and docs</h5>
        <p>
            <h6>Package can be access through composer package website and Git repository:</h6>
            <ul>

            </ul>
            <li><a href="https://gitlab.com/atoman08/atoman_access_code" class="text-blue-500">Gitlab</a></li>
            <li><a href="https://packagist.org/packages/atoman/access-code" class="text-blue-500">Packagist</a><li>
        </p>
        <h5>Installing atoman\access-code</h5>

        <p>The recommended way to install atoman\access-code is through
        <a href="https://getcomposer.org/">Composer</a>.

        <div><code>composer require atoman/access-code</code></div>

        </p>

    <h5>Instruction after requiring the package</h5>

    <p>
        Check if the package is automatically discovered on your Laravel application.
        <ul>
            <li>Navigate to config/app.php.</li>
            <li>Check if <code>Atoman\AccessCode\AccessCodeServiceProvider::class,</code> is added into your Provider section.</li>
            <li>Paste <code>Atoman\AccessCode\AccessCodeServiceProvider::class,</code> inside you provider section if not already added.</li>
            <li>Go to the root of your Application, on terminal/Command Prompt, run <code>php artisan vendor:publish -- force</code> to help publish some publishable files like Migration file and config of the package.</li>
            <li>Check for Provider <code>Atoman\AccessCode\AccessCodeServiceProvider</code> to be published to your application.</li>
            <li>Select <code>Atoman\AccessCode\AccessCodeServiceProvider</code> to publish accessCode.php and Migration files.</li>
            <li>Run migration <code>php artisan migrate</code> to migrate package migration file to your database.</li>
        </ul>
    </p>

    <h5> Usage Intructions</h5>

    <div>
        <pre>
        <code>
            use Atoman\AccessCode\App\Http\Services\Access;

            public function (Access $access){
                $accessCode = $access->accessCode();     // Generate access code (default length 6)
                $accessCode = $access->accessCode(8);     // Generate access code (defining length 8 ) Hint: defined length must always be >= default 6
            }

        </code>
        </pre>
    </div>


    <h6> Adjustment to default rules when generating Code </h6>
    <div>
        <pre>
        <code>
            use Atoman\AccessCode\App\Http\Services\Access;

            public function (){
                $accessCode = new Access();

                $accessCode->setCharacterRepeat(4);                     //setting value for repetition character
                $accessCode->setCharacterUniqueness(4);                 //setting value for uniqueness
                $accessCode->setPalindrome(false);                      // set if palindrome should be check
                $accessCode->setSequenceLength(4);                      //set length of sequence

                $accessCode = $access->accessCode(8);                    // Generate access code (defining length 8 )
            }

        </code>
        </pre>
    </div>
    <h5>Accessing the Interface</h5>
    <p>
        You can access the interface for the package while your application is running, navigate to your /access to view the interface. You will have access to Documentation page, Create Code Page, List all code page, Confirm Code page, Assign Code page.
    </p>

    <h5>Version Guidance</h5>

    <p>
        <ul>
            <li><a href="https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.2.0?ref_type=tags">atoman-access-code-repo v1.2.0</a></li>
            <li><a href="https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.0.2?ref_type=tags">atoman-access-code-repo v1.0.2</a></li>
            <li><a href="https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.0.1?ref_type=tags">atoman-access-code-repo v1.0.1</a></li>
            <li><a href="https://gitlab.com/atoman08/atoman_access_code/-/tree/v1.0.0?ref_type=tags">atoman-access-code-repo v1.0.0</a></li>
        </ul>
    </p>

    <h5>Security</h5>

    <p>
        If you discover a security vulnerability within this package, please send an email to atoworldinc@gmail.com or ato4life2008@gmail.com. All security vulnerabilities will be promptly addressed. Please do not disclose security-related issues publicly until a fix has been announced.
    </p>
    <h5>License<h5>
    <p>atoman\access-code is made available under the MIT License (MIT). Please see [License File](LICENSE) for more information.</p>


    </div>
@endsection
